
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

</head>

<body>
<h1><center>
    Testing Is Fun DNS Tools
</center></h1>
<fieldset>
<legend><b>DNS Lookup<b></legend>
<form action="dns_lookup.php" method="get" name="dns_lookup" target="_blank">
  <table width="484" border="0">
    <tr>
      <td width="148">Domain Name</td>
      <td width="5">&nbsp;</td>
      <td width="249"><label>
        <input type="text" name="domain" id="domain" />
      </label></td>
    </tr>
    <tr>
      <td>Record Type</td>
      <td>&nbsp;</td>
      <td><select name="record" id="record">
        <option>A</option>
        <option>MX</option>
        <option>CNAME</option>
        <option>SOA</option>
        <option>AAAA</option>
        <option>NS</option>
        <option>PTR</option>
        <option>TXT</option>
        <option>ANY</option>
        <option>HINFO</option>
      </select></td>
    </tr>
    <tr>
      <td><label>
        <input type="submit" name="DNS_Lookup" id="DNS_Lookup" value="Submit" />
        <input type="reset" name="button" id="button" value="Reset" />
      </label></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
  
  
  
  
</fieldset><br />
<fieldset><legend>DNS Lookup On Server</legend>
<form action="dns_srv_lookup.php" method="get" name="dns_srv_lookup" target="_blank">
  <table width="484" border="0">
    <tr>
      <td width="148">Domain Name</td>
      <td width="5">&nbsp;</td>
      <td width="249"><label>
        <input type="text" name="domain" id="domain" />
      </label></td>
    </tr>
    <tr>
      <td>Server Name</td>
      <td>&nbsp;</td>
      <td><input type="text" name="server" id="server" /></td>
    </tr>
    <tr>
      <td>Record Type</td>
      <td>&nbsp;</td>
      <td><select name="record2" id="record2">
        <option>A</option>
        <option>MX</option>
        <option>CNAME</option>
        <option>SOA</option>
        <option>AAAA</option>
        <option>NS</option>
        <option>PTR</option>
        <option>TXT</option>
        <option>ANY</option>
        <option>HINFO</option>
      </select></td>
    </tr>
    <tr>
      <td><label>
        <input type="submit" name="DNS_srv_Lookup" id="DNS_srv_Lookup" value="Submit" />
        <input type="reset" name="button" id="button" value="Reset" />
        </label></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
</fieldset><br />


<fieldset>
  <legend>Whois Lookup</legend>
<form action="whois_lookup.php" method="get" name="whois_lookup" target="_blank">
  <table width="484" border="0">
    <tr>
      <td width="148">Domain Name</td>
      <td width="5">&nbsp;</td>
      <td width="249"><label>
        <input type="text" name="domain" id="domain" />
      </label></td>
    </tr>
   
    <tr>
      <td><label>
        <input type="submit" name="whois_lookup" id="whois_Lookup" value="Submit" />
        <input type="reset" name="button" id="button" value="Reset" />
      </label></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
</fieldset>
<fieldset>
  <legend>Http Header</legend>
<form action="http_header.php" method="get" name="http_header" target="_blank">
  <table width="484" border="0">
    <tr>
      <td width="148">Domain Name</td>
      <td width="5">&nbsp;</td>
      <td width="249"><label>
        <input type="text" name="domain" id="domain" />
      </label></td>
    </tr>
   
    <tr>
      <td><label>
        <input type="submit" name="http_header" id="http_header" value="Submit" />
        <input type="reset" name="button" id="button" value="Reset" />
      </label></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form></fieldset>
<fieldset>
  <legend>OpenSSL Connect</legend>
<form action="openssl_connect.php" method="get" name="openssl_connect" target="_blank">
  <table width="484" border="0">
    <tr>
      <td width="148">Domain Name</td>
      <td width="5">&nbsp;</td>
      <td width="249"><label>
        <input type="text" name="domain" id="domain" />
      </label></td>
    </tr>
   
    <tr>
      <td><label>
        <input type="submit" name="openssl_connect" id="openssl_connect" value="Submit" />
        <input type="reset" name="button" id="button" value="Reset" />
      </label></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
</fieldset>
</body>
</html>
